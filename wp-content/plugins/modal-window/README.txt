﻿=== Plugin Name ===
Contributors: Wpcalc
Donate link: https://wow-estore.com/
Tags: 	animated popup, auto close, delayed popup, image popup, lightbox, modal plugin, modal popup, modal window, modal windows, onclick popup, popup, popup plugin, popup window, popups
Requires at least: 3.2
Tested up to: 4.6
Stable tag: 1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily create a modal window (popup)

== Description ==
This plugin lets you easily create a modal window (also known as a popup) and place it wherever you want on your Wordpress site with a shortcode.

[Get Wow version here](https://wow-estore.com/downloads/wow-modal-windows/)

= Demo =
[Modal Window demo](http://free.marketing-wp.com/mwp-modal-windows)

[youtube https://www.youtube.com/watch?v=Yk7XYub_d0Q]

= Trigger the modal window on: = 
* Click on a link or button
* Opening the page
* Scrolling the page
* When the user tries to navigate away from the page (moves the cursor away from your page and tries to close or switch tabs, for example)

= Enable timing: = 
* Delay the modal window show up

= Show modal window: = 
* All the time
* Only once
* Show again after the number of days you specify

= Insert any content: = 
* HTML Text
* Banner Ads
* Image or Image Gallery
* Video or Video Gallery
* Audio or Audio Gallery
* Pdf viewer
* Iframe Content
* Forms
* SlideShare Content
* and other media
 
= Modal windows can be used for: = 
* Advertisements
* Contact forms
* Disclaimer contents
* Notifications
* Alert messages
* Product description
* Work portfolio
* Product images
* Google maps
* Notices
* Other information

= Other notes: = 
* Works on mobiles
* Only 3 modal windows is possible in this free version
* There isn’t a trigger on page close

= Use with our other tools: = 

Use this plugin with the other tools of the Wow-company to create any kind of marketing widgets for your website!

* [Wow Countdowns](https://wow-estore.com/downloads/wow-countdowns/)
* [Wow Forms](https://wow-estore.com/downloads/wow-forms/)
* [Wow Herd Effects](https://wow-estore.com/downloads/wow-herd-effects/)
* [Wow Modal Windows](https://wow-estore.com/downloads/wow-modal-windows/)
* [Wow Side Menus](https://wow-estore.com/downloads/wow-side-menus/)
* [Wow Skype Buttons](https://wow-estore.com/downloads/wow-skype-buttons/)
* [Wow Signups](https://wow-estore.com/downloads/wow-signups/)


= Translations =
* Russian
* English

= Support =
[Facebook](https://www.facebook.com/wowaffect/)

== Installation ==

* Download the folder modal-window in the plugins directory `/wp-content/plugins/`
* Activate «Modal Window» in the “Plugins” section.
* Go to plugin setup section and install the necessary options.

== Screenshots ==
1. Example.
2. Example.
3. Admin.

== Changelog ==

= 1.3 = 
* Fixed code
* Change style

== Changelog ==
= 1.2.1 = 
* Edited contacts

= 1.2 = 
* Fixed display a modal window


= 1.1 = 
* Fixed display a modal window
* Add option closing modal window


= 1.0 = 
* Initial release