<?php
if ( ! defined( 'ABSPATH' ) ) exit; 
function show_modalsimple_window($atts) {
    extract(shortcode_atts(array('id' => ""), $atts));	
    global $wpdb;
	$table_modal = $wpdb->prefix . "modalsimple";
    $sSQL = "select * from $table_modal WHERE id=$id";
    $arrresult = $wpdb->get_results($sSQL); 
    if (count($arrresult) > 0) {
        foreach ($arrresult as $key => $val) {
			include_once( 'partials/public.php' );
			ob_start();
			if ($val->use_cookies == 'yes' ){
				include_once( 'js/cookies-js.php' );
			}
			else {
				include_once( 'js/custom-js.php' );
			}
			$content_script = ob_get_contents();
			$packer = new JavaScriptPacker($content_script, 'Normal', true, false);
			$packed = $packer->pack();					
			ob_end_clean();			
			echo '<script type="text/javascript" charset="utf-8"> '.$packed.'</script>';			
			include_once( 'css/custom-css.php' );			
						
			if ($val->use_cookies == "yes"){
				wp_enqueue_script( 'cookie-modal-window', plugin_dir_url( __FILE__ ) . 'js/jquery.cookie.js', array( 'jquery' ) );
			}			
        }
    } else {		
		echo "<p><strong>No Records</strong></p/><p/>";        
    }  
	
	return;
}
add_shortcode('modalsimple', 'show_modalsimple_window');