<link href="<?php echo plugins_url('styles/bootstrap.css', __DIR__); ?>" rel="stylesheet">
<div class="sabres-header">
    <?php
        $main_page = Sabres_Admin::get_menu_slug();
    ?>
    <a href="<?php menu_page_url( $main_page ); ?>" class="sabres-logo"></a>
</div>

<?php foreach($errors as $error): ?>
    <div class="error">
        <p><strong>Error</strong>: <?php echo $error; ?></p>
    </div>
<?php endforeach; ?>

<div class="sabres-tfa-form">
    <h1>Configure Two Factor Authentication</h1>
    <form class="validate" action="" method="post">
        <table class="form-table">
            <tr>
                <th scope="row">
                    Token delivery:
                </th>
                <td>
                    <fieldset>
                        <label>
                            <input type="radio" name="delivery" value="<?php echo SbrTfa::DELIVERY_TYPE_EMAIL; ?>"
                                <?php if ($settings['delivery'] == SbrTfa::DELIVERY_TYPE_EMAIL): ?> checked <?php endif; ?>
                            />
                            Send via Email
                        </label>
                        <br>
                        <label>
                            <input type="radio" name="delivery" value="<?php echo SbrTfa::DELIVERY_TYPE_SMS; ?>"
                                <?php if ($settings['delivery'] == SbrTfa::DELIVERY_TYPE_SMS): ?> checked <?php endif; ?>
                            />
                            Send via SMS
                        </label>
                        <br>
                        <label>
                            <input type="radio" name="delivery" value="<?php echo SbrTfa::DELIVERY_TYPE_BOTH; ?>"
                                <?php if ($settings['delivery'] == SbrTfa::DELIVERY_TYPE_BOTH): ?> checked <?php endif; ?>
                            />
                            Both
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="sbr_tfa_email">Email:</label>
                </th>
                <td>
                    <input type="email" name="email" class="regular-text" placeholder="some@example.com"
                           value="<?php echo isset($settings['email']) ? $settings['email'] : ''; ?>"
                    />
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="sbr_tfa_phone">Phone:</label>
                </th>
                <td>
                    <input type="tel" name="smsNumber" class="regular-text" placeholder="+15552341234"
                           value="<?php echo isset($settings['smsNumber']) ? $settings['smsNumber'] : ''; ?>"
                    />
                    <p class="description">(with country code)</p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    Strictness:
                </th>
                <td>
                    <fieldset>
                        <label>
                            <input type="radio" name="strictness" id="rad-new-device" value="<?php echo SbrTfa::STRICTNESS_TYPE_NEW_DEVICE; ?>"
                                <?php if ($settings['strictness'] == SbrTfa::STRICTNESS_TYPE_NEW_DEVICE): ?> checked <?php endif; ?>
                            />
                            Require two factor for new devices / browsers.
                        </label>
                        <br>
                        <label>
                            <input type="checkbox"  id="chk-device-expiry"
                            <?php if ($settings['device-expiry-checked']): ?> checked <?php endif; ?>
                            />
                            <input type="hidden" name="device-expiry-checked" id="hid-device-expiry"  value="<?php echo $settings['device-expiry-checked']; ?>"
                            />
                            Devices need to reauthenticate via two factor after <input type="number" id="txt-expiry-days" name="device-expiry-days"
                              value="<?php echo $settings['device-expiry-days']; ?>" maxlength="3" min="1" max="999" size="3"/>  days.
                        </label>
                        <br>
                        <label>
                            <input type="radio" name="strictness" id="rad-every-login" value="<?php echo SbrTfa::STRICTNESS_TYPE_EVERY_LOGIN; ?>"
                                <?php if ($settings['strictness'] == SbrTfa::STRICTNESS_TYPE_EVERY_LOGIN): ?> checked <?php endif; ?>
                            />
                            Every login requires two factor authentication.
                        </label>
                    </fieldset>
                </td>
            </tr>
        </table>
        <p class="submit">
            <table class="form-table">
              <tr>
                  <th><input type="submit" name="submit" class="button button-primary" value="Save Settings"/></th>
                  <td><input type="reset" name="reset" class="button button-secondary" value="Cancel"/></td>
              </tr>
            </table>
        </p>
    </form>
</div>
<script type="text/javascript">
var sabres=sabres || {};
sabres.tfa={};

(function(window, document,tfa){

  var radNewDevice=document.getElementById("rad-new-device");
  var radEveryLogin=document.getElementById("rad-every-login");
  var chkDeviceExpiry=document.getElementById("chk-device-expiry");
  var txtExpiryDays=document.getElementById("txt-expiry-days");
  var hidDeviceExpiry=document.getElementById("hid-device-expiry");

  var evalReadonlyElements=function() {
    if (radNewDevice.checked) {
      chkDeviceExpiry.disabled=false;
      if (chkDeviceExpiry.checked)
        txtExpiryDays.readOnly=false;
      else
       txtExpiryDays.readOnly=true;
    }
    else {
      chkDeviceExpiry.disabled=true;
      txtExpiryDays.readOnly=true;
    }
  }

  radNewDevice.onchange=evalReadonlyElements;
  radEveryLogin.onchange=evalReadonlyElements;
  chkDeviceExpiry.onchange=function() {
    if (chkDeviceExpiry.checked)
       hidDeviceExpiry.value='true';
    else
       hidDeviceExpiry.value='false';
    evalReadonlyElements();
  };

  chkDeviceExpiry.onchange();


})(window, document,sabres.tfa);
</script>
