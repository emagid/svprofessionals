
	<div id="secondary" class="secondary">

		<div id="widget-area" class="widget-area" role="complementary">
			<h2 class="widget-title">Recent Posts</h2>	

			<ul>

			<?php
	  			$args = array(
	    		'post_type' => 'blog',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<li>
					<a href="#<?php the_field('title'); ?>"><?php the_field('title'); ?></a>
				</li>

				<?php
			}
				}
			else {
			echo 'No Recent Posts Found';
			}
		?>
			</ul>
		</div><!-- .widget-area -->
		
	
	</div><!-- .secondary -->

